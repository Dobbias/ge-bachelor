\chapter{Solving Network Problems}\label{solving_net_probs}

This chapter will show how the problems from the previous chapter are solved in PolyNet. The first part of the solution consists of a general explanation on how to solve the problem. The second shows how the library's implementation tries to realize the former solutions. Besides the article series by Gabriel Gambetta (cited later on), the following two sources were used for general information retrieval: \cite{gaffer} and \cite{beej}.

\section{Server Broadcasting Delay}

This section will introduce two ways to solve the problem illustrated in \autoref{fig:serverdelay}. Both methods described here should be used in order to minimize the effect of the server broadcasting delay.

\subsection{Client-Side Prediction}\label{sub:client_prediction}

To solve the situation, that the inputs of a client are only displayed after the server sent out a new snapshot, we can introduce a local simulation of the game running at every client. Generally we can assume every input to be executed as expected, since the server only declines inputs that are not valid (e.g. result of modifying packets/hacking) or if the player's ping is too high.
\par 
We can directly apply the inputs to the local simulation and the client will render the correct view. That is the case until the next snapshot arrives from the server. Since the gamestates sent by the server are used to authoritatively correct the local view, the local simulation will be set to the state of the last accepted input. This would undo all inputs that occurred after the snapshot was sent out. Those dropped inputs will only be acknowledged in the next snapshot. That would result in a client jumping back and forth. \cite{gambetta2}

\subsection{Server Reconciliation}

The dropping of local, not acknowledged inputs can be circumvented by the following. We cache all locally performed inputs in a buffer. Those inputs are timestamped like the snapshots. When receiving a snapshot the client can delete all inputs older than the end time of the snapshot. Which means it will keep the non acknowledged inputs and simulate them on top of the received snapshot. This way all non valid inputs are dropped after a newer snapshot arrives and the client's view still matches the expected live view including non acknowledged events. \cite{gambetta2}

\subsection{Implementation Details}

For the local simulation PolyNet will cache inputs triggered in the current frame after sending them to the server. Then after updating the remote clients in the local view (see \autoref{lst:client_input}), it will immediately display the current frame's inputs. It expects the server accepting the sent out inputs in most cases.

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:client_input}, caption={Client-Side Input Processing}]
void SimServer::clientTick(float delta) {

  // Sends out inputs to server
  for (auto& x : cCurrentFrameInputs_) {
    // Set Time & ClientId
    x->ClientId = this->client_->ClientId;
    x->TimeStamp = time_;
    SendInput(shared_ptr<Input>(x));
    cInputCache_.push_back(x);
  }
  cCurrentFrameInputs_.clear();

  // ... Code for updating other clients ...
  
  // Update only local player
  multiStepUpdateSnapshot(time_ - cOtherUsersTime_,
                          vector<uint8_t>{client_->ClientId}, mergedInputs,
                          cActiveSnapshot_.get(), false, false,
                          cLocalClientEventsProcessed_);
}
\end{lstlisting}

\noindent
After a new snapshot is set to the currently active one, all non-acknowledged inputs will be dropped as shown in \autoref{lst:input_drop}. This will cause the client to accept the server's snapshot, as required.

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:input_drop}, caption={Dropping of Old Inputs}]
void SimServer::clientTick(float delta) {

  // ...

  // Delete old inputs if new snapshot arrives
  cInputCache_.erase(
    std::remove_if(cInputCache_.begin(), cInputCache_.end(),
      [&](shared_ptr<Input>& element) {
        return element->TimeStamp <
        currentSnapshot_->TimeStamp;
       }),
    cInputCache_.end());
    
  // ...
}
\end{lstlisting}

\section{Network Input Delay}\label{sec:inputdelay}

Displaying the other clients correctly at the local client is a crucial step to create an immersive experience for the players. This section will show how this is achieved in this implementation.

\subsection{Caching Snapshots}

The key to solving the problem is to realize, that there is no way for a client to show what the players are doing at the same point in time. If the local client would only interpolate the other clients' states until receiving a snapshot, this would result in wrong movements. Instead of jumping entities we want to display live which inputs the players are performing during snapshot updates.
\par
To do this we need to cache server snapshots locally. While still showing the local player's actions live, PolyNet will display what inputs the other players performed between two cached snapshots. This way we can show pseudo-live inputs since all inputs between cached snapshots are known. Also the other players are always displayed in the past.
\par
For this to work properly caching two snapshots would be enough. But what happens if the ping of the local player suddenly rises and the next server snapshot arrives later than expected? The local player would have no snapshots to display further movement of other players. The local players movement would still be fine since its inputs would be applied to the local simulation. To solve this problem PolyNet can cache an user defined amount of snapshots. This, of course, is a trade-off between showing the other players further in the past and being able to handle ping fluctuations.
\par
The next section (\autoref{sec:time_cruc}) will help to handle the offset between the local player and the other players states.

\subsection{Implementation Details}

PolyNet fills a buffer with snapshots that will be used for displaying the other client's actions. If the buffer is filled the local simulation will start displaying the past events (see \autoref{lst:client_past}). PolyNet has to handle different scenarios. The update delta could overlap with two snapshots and this results in the code below. The current snapshot has to be set accordingly if the simulation surpasses the end time of the previous current snapshot. When changing the current snapshot all old local inputs will be dropped like shown before.

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:client_past}, caption={Remote Inputs Processing}]
void SimServer::clientTick(float delta) {

  // ... Process local inputs ...

  if (cFilledPrefetchBuffer_) {
    cOtherUsersTime_ += delta;
    float currentTime = currentSnapshot_->TimeStamp;
    cActiveSnapshot_ = getCurrentSnapshotCloned();

    while (currentTime < cOtherUsersTime_) {
      bool inNextSnap =
          currentSnapshot_->TimeStamp + currentSnapshot_->Duration <
          cOtherUsersTime_;
      bool doNextSnap = inNextSnap && cReceivedSnapshots_.size() > 1;

      if (inNextSnap) {
        // End in next snap
        mergedInputs = mergeInputLists(cInputCache_, currentSnapshot_->Inputs);
        multiStepUpdateSnapshot(
            (currentSnapshot_->TimeStamp + currentSnapshot_->Duration) -
                currentTime,
            GetAllClientIds(cActiveSnapshot_.get()), mergedInputs,
            cActiveSnapshot_.get(), true, true);
        currentTime = currentSnapshot_->TimeStamp + currentSnapshot_->Duration;

        if (doNextSnap) {
          // Save current snap in next snap
          cActiveSnapshot_->TimeStamp = cReceivedSnapshots_.at(1)->TimeStamp;
          cActiveSnapshot_->Duration = cReceivedSnapshots_.at(1)->Duration;
          cActiveSnapshot_->Inputs = cReceivedSnapshots_.at(1)->Inputs;
          cReceivedSnapshots_.erase(cReceivedSnapshots_.begin());
          cReceivedSnapshots_.at(0) = cActiveSnapshot_;
          currentSnapshot_ = cReceivedSnapshots_.at(0);
          cActiveSnapshot_ = getCurrentSnapshotCloned();

          // ... Delete old inputs ...
        } else {
          // Quit loop since there is no buffer to display
          currentTime = cOtherUsersTime_;
        }
      } else {
        // End in current snap
        mergedInputs = mergeInputLists(cInputCache_, currentSnapshot_->Inputs);
        multiStepUpdateSnapshot(cOtherUsersTime_ - currentTime,
                                GetAllClientIds(cActiveSnapshot_.get()),
                                mergedInputs, cActiveSnapshot_.get(), true,
                                true);
        // Quit loop
        currentTime = cOtherUsersTime_;
      }
    }
  }

  //  ... Update local client ... 
  
}
\end{lstlisting}

\section{Time Crucial Events}\label{sec:time_cruc}

One of the most important problems to solve, especially for fast paced games, is the correct handling of time crucial events. There are generally two ways to solve the problem in \autoref{fig:timecrucial}. We will take a look at them in \autoref{sub:resolvetime}. In \autoref{sub:limit_accpetance} a general recommendation is explained. Since we saw in the last section, that we have to display other players in the past, a way of resolving different player views is important.

\subsection{Limiting Input Acceptance}\label{sub:limit_accpetance}

In PolyNet the server will collect inputs for a snapshot for a userdefined timespan. Those inputs will be then included in the next snapshot and displayed at the clients as the other players' inputs. Since you don't want to increase the delay by a large time amount, it is a good idea to keep this timespan small (e.g. 100 ms). The user of PolyNet may want to still accept inputs older than 100 ms, maybe 500 ms. All inputs between 100 ms and 500 ms will not be shown pseudo-live at the other clients. They will be included in the snapshot state (already applied). Setting the maximum input delay acceptance to a lower value will make feel the actions more immediate, but drop inputs for clients with a higher ping. This technique is not a direct solution to the problem, but can be used as an additional option for certain games.

\subsection{Calculating Player States on the Server}\label{sub:resolvetime}

There are two general ways to resolve time crucial events. As we can see in \autoref{fig:timecrucial} the actor wants the bullet hit to be triggered, since it is, what its view shows. But for the receiver it would weird to die, because in his/her view the bullet is not close to him/her. The example in the figure may be extreme, but it can still be noticeable, if both players have a high ping.
\par
One way would be simply not solving this problem, which would satisfy the receiver. The other way would be to resolve this situation and make the actor happy. Unfortunately there is no way to satisfy both clients' needs at the same time.
\par
PolyNet takes the second approach. The reason why is, that it is expected to be more satisfying hitting a target when it is visually happening instead of not getting acknowledged the hit/event. Getting hit for no reason is weird, but may be a good trade for getting rewarded when being the actor. 
\par
Since the server has all the snapshots it has sent out to every client, all inputs send from the player and the clients local snapshot cache (for displaying other players "live"), it is able to calculate every client's individual view. The library will run the "UpdateClientAndRelatedObjects" and "UpdateSnapshotWithoutClients" method with a flag set indicating, whether to trigger important events. This methods are used for the normal simulation as well for the client specific updates. If an event is triggered in the client view on the server, PolyNet will apply the event on the normal server snapshot effectively merging the client specific events with the server view. \cite{gambetta4}

\subsection{Implementation Details}

As shown in \autoref{lst:server_rewind} the server calculates the clientViews and runs the simulation on them. All events will also be triggered on the worldState (the snapshot being send to all players). This merges the important client events with the present snapshot. 

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:server_rewind}, caption={Time Crucial Event Processing}]
// Pseudo-Code
void SimServer::serverTick(float delta) {
  // ... Process Inputs ...
  vector<Snapshot*> clientViews = CalculateClientViews();
  Snapshot* worldState = GetCurrentSNapshot();
  foreach(auto& clientView : clientsViews) {
    multiStepUpdateSnapshot(delta, clientView, worldState, noClientEvents);
  }
}
\end{lstlisting}