\chapter{Library Usage Sample}\label{c:lib_useage_sample}

This appendix chapter will show how a user of PolyNet would use the library to create a simple multiplayer game. It will include code excerpts from the sample project. Only PolyNet related code will be shown and not how to setup a gameloop for the clients or how the game is rendered with SFML.

\section{Game Idea}

The game idea is a simple one, but includes all features of PolyNet. The basic gameplay will involve two players who are able to shoot bullets. Both players have got three lifes. If a player is hit by an enemy's bullet his life counter will decrease by one. If one player's life counter reaches 0, the other player won and the game will quit. This will demonstrate how to process and delay inputs, handle the server delay and how to handle time crucial events. For simulating remote clients and server the built-in fake latency functionality will be used.

\section{Using the Library}

For using the library you first need to add it as dependency to your project. In \autoref{lst:basic} you can see how to initialize and de-initialize PolyNet. Also it includes setting up a server and two clients for the sample project.

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:basic}, caption={Basic PolyNet Usage}]
  // Init PolyNet
  if (!polynet::PolyNet::Init()) {
    this->currentGame_->SetRunning(false);
  }

  // Setup server and two clients
  server_ = make_unique<polynet::Server>(
      1203, std::move(unique_ptr<SimServer>(new MySimServer(true, 1.0f / 60))),
      serverUpdateTime, maxPingAllowed, maxLiveInputDelay);
  clientOne_ = make_unique<polynet::Client>(
      1203, "127.0.0.1",
      std::move(unique_ptr<SimServer>(new MySimServer(false, 1.0f / 60))),
      0.05f);
  clientOne_->SetFakeLatency(0.05f);
  clientTwo_ = make_unique<polynet::Client>(
      1203, "127.0.0.1",
      std::move(unique_ptr<SimServer>(new MySimServer(false, 1.0f / 60))),
      0.05f);
  clientTwo_->SetFakeLatency(0.09f);
  clientOneSimServer_ = (MySimServer*)clientOne_->SimServerInstance.get();
  clientTwoSimServer_ = (MySimServer*)clientTwo_->SimServerInstance.get();

  server_->RunInSeperateThread(serverUpdateTime);
  
  // ... gameloop in between ...
  
  polynet::PolyNet::Destroy();
}
\end{lstlisting}

The library has to be instantiated with a call to PolyNet::Init(), this will return a boolean indicating, whether the call was successful or not. In the end the user has to call PolyNet::Destroy() to release resources.
\par
Both the clients and the server need instances of the custom SimServer class. Furthermore there are fake latencies specified for local testing. The SimServer instance also is created using a 16 ms second delta for internal calculations like rerunning the simulation. Also the server will be running this game at the port 1203. The server instance is created by specifying several other parameters that include the server tick-rate, the maximum ping allowed for accepting inputs and a maximum ping for transmitting the inputs to other clients as a pseudo-live view. In this example a built-in function is used for running the server in a separate thread. This is used in the sample since the tick-rate of the clients and the server should be different.

\subsection{Creating a Custom Input Class}

Before creating a gamestate model in form of a custom Snapshot class, the user should define all inputs that will be used in the game. In our sample game we only need keyboard input. Five keys should be enough: four directions and one input for shooting a bullet. A byte for the key and boolean for the state are enough. Since the Input class is a child class of the NetObject class we need to specify ways to clone, serialize and de-serialize the object. In \autoref{lst:custominput} you can explicitly see those methods implemented. For the following guide this methods won't be shown fully implemented since it only demonstrates the usage of built-in methods. Because PolyNet wants to be highly configurable you also have to read/write the data fields of the parent classes by your own (e.g. input timestamp).

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:custominput}, caption={Custom Input Class}]
class KeyboardInput : public Input {
 public:
  char Key;
  bool Down;

  KeyboardInput(char key = -1, bool down = false)
      : Input(/*KeyboardInputPacket::ID*/ 1000) {
    this->Key = key;
    this->Down = down;
  }

  uint32_t GetNetSize() {
    return sizeof(uint8_t) + sizeof(float) + sizeof(uint8_t) + sizeof(uint8_t);
  }

  void Serialize(Packet* packet) {
    packet->s_byte(ClientId);
    packet->s_float(TimeStamp);
    packet->s_byte(Key);
    packet->s_bool(Down);
  }

  void Deserialize(Packet* packet) {
    ClientId = packet->d_byte();
    TimeStamp = packet->d_float();
    Key = packet->d_byte();
    Down = packet->d_bool();
  }

  shared_ptr<NetObject> Clone() {
    shared_ptr<KeyboardInput> result = std::make_shared<KeyboardInput>();
    result->ClientId = ClientId;
    result->TimeStamp = TimeStamp;
    result->Key = Key;
    result->Down = Down;
    return result;
  }
};
\end{lstlisting}

\subsection{Creating a Custom Snapshot Class}

The custom snapshot class will contain a gamestate for the sample game. This will include two players and a vector of bullets.
The Player and Bullet class won't be shown here since they are only simple NetObjects containing data like size, position, speed, lifes.
The custom snapshot will basically only be a plain data container. In the sample game's case it will only contain other game objects, see \autoref{lst:customsnapshot}.

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:customsnapshot}, caption={Custom Snapshot Class}]
class CustomSnapshot : public Snapshot {
 public:
  std::shared_ptr<Player> player1 = std::make_shared<Player>(),
                          player2 = std::make_shared<Player>();
  std::vector<std::shared_ptr<Bullet>> bullets;

  uint32_t GetNetSize() {
  	// straightforward code
  }

  void Serialize(Packet* packet) {
  	// straightforward code
  }

  void Deserialize(Packet* packet) {
  	// straightforward code
  }

  shared_ptr<NetObject> Clone() {
  	// straightforward code
  }
};
\end{lstlisting}

\subsection{Creating Custom Packets}

Since the custom packets in the sample project only include a net-object at their root, the subsection will only show how to implement one packet. The other packets are mostly the same excluding the packet id. As an example you can see the CustomKeyboardInputPacket class in \autoref{lst:custominputpkt}.

\clearpage

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:custominputpkt}, caption={Custom Input Packet Class}]
class KeyboardInputPacket : public Packet {
 public:
  static const uint16_t ID = 1000;

  shared_ptr<KeyboardInput> Input = std::make_shared<KeyboardInput>();

  KeyboardInputPacket() : Packet(ID){};

  uint8_t* Serialize(int& outSize) {
    uint8_t* outData = getBuffer(Input->GetNetSize(), outSize);
    Input->Serialize(this);
    return outData;
  }

  void Deserialize(uint8_t* data) {
    setBuffer(data);
    Input->Deserialize(this);
  }
};
\end{lstlisting}
\noindent
As you can see the class contains an unique id identifying the packets. It only consists of an input net-object and uses the Serialize/Deserialize function to write the object to the data buffer.
\par
With all the custom data objects we can start to implement the logic in a custom SimServer class now.

\subsection{Creating a Custom SimServer}

Implementing a custom sim-server is providing all the game logic and required functions to PolyNet. See \autoref{lst:customsim} for the sample project's sim-server. The most important functions will be explained after the following listing.

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:customsim}, caption={Custom SimServer Class}]
MySimServer::MySimServer(bool serverMode, float fineDeltaBetweenInputs)
    : SimServer(serverMode, fineDeltaBetweenInputs) {}

void MySimServer::sOnClientConnected(ServerClient* serverClient) {
  serverClient->AcceptConnect();

  // Start Sample Game
  if (server_->ServerClients.size() == 2) {
    OnStart();
    for (auto const& x : server_->ServerClients) {
      x.second->SendPacket(CreateSnapshotPacket(GetCurrentSnapshot()));
    }
  }
}

void MySimServer::OnPacketReceived(uint16_t packetId, uint8_t* data) {
  if (isServer_) {
    switch (packetId) {
      case KeyboardInputPacket::ID: {
        auto packet = std::make_shared<KeyboardInputPacket>();
        packet->Deserialize(data);
        sProcessIncomingInput(packet->Input);
        break;
      }
    }
  } else {
    switch (packetId) {
      case CustomSnapshotPacket::ID: {
        auto packet = std::make_shared<CustomSnapshotPacket>();
        packet->Deserialize(data);
        float xPos = packet->Snapshot->player2->PosY;
        cProcessIncomingSnapshot(packet->Snapshot);
        if (!GetIsRunning()) OnStart();
        break;
      }
      // Needed in every implementation
      case polynet::NetIdPacket::ID: {
        auto packet = std::make_shared<polynet::NetIdPacket>();
        packet->Deserialize(data);
        client_->CurrentNetId = packet->StartId;
        client_->ClientId = packet->ClientId;
        break;
      }
    }
  }
}

void MySimServer::OnStart() {
  SimServer::OnStart();

  if (isServer_) {
    auto snap = std::dynamic_pointer_cast<CustomSnapshot>(GetCurrentSnapshot());
    // Setup initial game state (before broadcasting it to the players)
  }
}

void MySimServer::ApplyInput(Input* input, Snapshot* snapshot,
                             bool clientEvents, Snapshot* rewindSnapshot) {

  switch (input->PacketId) {
  	// Apply input to provided snapshot e.g. change player speed
  }
}

void MySimServer::SendInput(shared_ptr<Input> input) {
  switch (input->PacketId) {
    case KeyboardInputPacket::ID: {
      auto packet = std::make_shared<KeyboardInputPacket>();
      packet->Input = std::dynamic_pointer_cast<KeyboardInput>(input);
      SendToServer(packet);
      break;
    }
  }
}

// Called by client and server to progress simulation in between inputs
void MySimServer::UpdateClientAndRelatedObjects(float delta, uint8_t clientId,
                                                bool clientEvents,
                                                polynet::Snapshot* snapshot,
                                                Snapshot* rewindSnapshot) {
  CustomSnapshot* snap = dynamic_cast<CustomSnapshot*>(snapshot);
  Player* player = snap->player1->ClientId == clientId ? snap->player1.get()
                                                       : snap->player2.get();

  // Move player & check collisions for player and associated bullets
}

vector<uint8_t> MySimServer::GetAllClientIds(polynet::Snapshot* snapshot) {
  // Return all client ids in the snapshot
}

// Update world without client-specific code. e.g. updating some timers ...
void MySimServer::UpdateSnapshotWithoutClients(float delta, bool clientEvents,
                                               bool logicEvents,
                                               Snapshot* snapshot) {}

shared_ptr<Snapshot> MySimServer::CreateSnapshot() {
  // Instantiate new custom snapshot
}

shared_ptr<Packet> MySimServer::CreateSnapshotPacket(
    shared_ptr<Snapshot> snapshot) {
    // Create new custom snapshot packet from snapshot
}
\end{lstlisting}
\noindent

\begin{description}
\item [sOnClientConnected] In the sample the server starts a game when 2 players have connected to server. It has to call OnStart() and then send out the first snapshot.
\item [OnPacketReceived] This function handles receiving packets. By using the isServer\_ bool it switches between handling the packets as server or client. In the example the server only processes input packets and the clients snapshot and net-id packets. This is the minimal required setup for PolyNet to work. The inputs and snapshots are redirected to other PolyNet functions directly.
\item [OnStart] Is called by the server when the game should start. This should initialize the initial gamestate like setting player positions, sizes and lives.
\item [ApplyInput] Sets the state of the snapshot after the input. In the sample it sets player speed or creates bullets. If the "rewindSnapshot" parameter is set, apply time crucial events to it aswell.
\item [SendInput] This function is called by PolyNet when an input should be put into a packet. Since the sample only uses the KeyboardInputPacket this is all code required.
\item [UpdateClientAndRelatedObjects] This function should progress the snapshots only for the client with the provided id and all objects created by this client. In the sample it moves the provided player and the bullets created by it. Here you could create callbacks to your application e.g. for playing sound. Events like sounds/animations should only be triggered here if the clientEvents flag is set. This is required since PolyNet reruns the simulation multiple times but you only want to trigger client events a single time. If the "rewindSnapshot" parameter is set, apply time crucial events to it aswell.
\item [UpdateSnapshotWithoutClients] This should only update entities not related to any player. It is not used in the sample game.
\item [GetAllClientIds] This extracts all client ids from the snapshot. Right here the id of player one and two.
\end{description}

\subsection{Sending Input from a Normal GameLoop}

Sending input to the simulation/PolyNet is pretty straightforward. From your gameloop's update function call SendInput on the client's sim-server. This will process the input locally and remotely. See \autoref{lst:inputpass} for reference. Process all the inputs before calling update on the sim-server!

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:inputpass}, caption={Passing Inputs to PolyNet}]
void GameScreen::Update(float delta) {
  // ... Other inputs and PolyNet unrelated updates ...

  if (IsKeyPressed(Direction::Up))
    clientSimServer_->ProcessInput(
        make_shared<KeyboardInput>(Direction::Up, true));

  // Update SimServer (see next subsection) ...
}
\end{lstlisting}
\noindent
The user instantiates a new KeyboardInput, when the key is pressed and passes it to PolyNet. In our sample project an input will be also send, if a key is released.

\subsection{Updating the Client}

Updating the client is simple. The user just has to call the Update(float delta) function on the client's sim-server. The delta is the time in seconds since the last update.

\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:clientupdate}, caption={Updating PolyNet Client}]
void GameScreen::Update(float delta) {
  // Process inputs ...
   clientSimServer_->Update(delta);
}
\end{lstlisting}
\noindent
This will perform any necessary work to update the local simulation and the network.
\subsection{Rendering the Current State of the Local SimServer}

For rendering the current state you only need to call GetCurrentSnapshot() on the sim-server and cast it to your custom snapshot class. Then just access all data in it and render the frame accordingly. 
\clearpage
\begin{lstlisting}[language=C++, backgroundcolor=\color{black!5}, basicstyle=\small,
                   label={lst:rendersnap}, caption={Retrieving the current Snapshot for Rendering}]
void GameScreen::Render(sf::RenderWindow* window) {
  auto snapshot = std::dynamic_pointer_cast<CustomSnapshot>(
      clientSimServer_->GetCurrentSnapshot());
        
  // ... rendering based on the snapshot ...
}
\end{lstlisting}

\noindent
This concludes the tutorial on how to use the library. For a more detailed implementation please take a look at the sample project.