\chapter{Library Architecture}\label{c:lib_arch}

This chapter will give an overview on the structure of the PolyNet library. Also it will explain what technology and existing work was used to
realize this work. 

\section{Technical Setup}

This section will show how PolyNet is build from source, what programming language was used and what external dependencies it has.

\subsection{Programming Language Used}

PolyNet is implemented using C++. Since there is a C++ compiler for every platform, this provides the maximum reach for going cross-platform. Also the resulting binary/library is smaller and faster than other technologies like scripting languages (e.g. Python) or \acrshort{jit}-compiler based languages (e.g. Java, C\#). As a language standard mostly C++ 11/14 was used.

\subsection{Libraries Used}

Since PolyNet is a high-level networking library, a low-level network library was used to build on top. The used features could be implemented into PolyNet, but this path has been chosen to finish this thesis in the given time. The only other library used is ENet. It provides features like reliable \acrfull{audp}, packet fragmentation /  reassembly and a way to send ordered packets. But PolyNet doesn't utilize all features it provides, this includes channels, as way of splitting requests into different types, which don't block each other by waiting for ordered packets.

\paragraph{Used Features Explained} \mbox{} \noindent

\begin{description}
\item [Reliable \acrshort{audp}] \acrshort{audp} by its nature doesn't acknowledge that a packet has been received on the other end of the connection. In the \acrfull{atcp} the receiver has to acknowledge the arrival of a packet. If this does not happen the sender resends the data. This of course introduces latency since the sender waits for the acknowledgment before it sends the next packet. Reliable \acrshort{audp} tries to add this feature to the "primitive" protocol while not adding the complexity of \acrshort{atcp}. This feature can compensate packet loss on loose connections by sacrificing latency.
\item [Packet Fragmentation/Reassembly] This is a feature to support sending data larger than the \acrfull{amtu}. This way PolyNet can simplify the sending of large packets like complete gamestate updates. ENet returns those packets as one data buffer while hiding the complexity of fragmenting the packet and reassembling it, keeping track of the fragment order and fragment loss.
\item [Ordered Packets] ENet returns packets in the correct order even when using \acrshort{audp} which itself is connectionless and therefore supports no packet ordering. ENet automatically adds sequence numbers to packets/fragments without exposing this to PolyNet.
\end{description}

\noindent
All those features reduce the work PolyNet has to do on the low-level network side.

\subsection{Supported Platforms}

PolyNet in theory supports the largest possible target group, since only a C++ compiler supporting C++14 is needed. Also the target system should
be supported by ENet. But ENet supports the majority of the common platforms. As stated on the project's website: "ENet works on Windows and any other Unix or Unix-like platform providing a BSD sockets interface."

\par This way you should be able to use PolyNet with every major non web technology by writing wrappers for the native library.
\par Due the time limitation, while creating PolyNet, it only got tested on Linux systems. Testing for other platforms will be performed in the future. But since PolyNet doesn't use platform specific functionality, there shouldn't be major problems on other platforms.

\subsection{Building the Library}

This project uses CMake\footnote{https://cmake.org}. It provides a cross-platform and cross-compiler/buildsystem way to create a project for compiling the source. On Windows with VisualStudio for example  CMake creates a VisualStudio Solution to compile, on UNIX systems with GCC\footnote{https://gcc.gnu.org} it will create a makefile. You can instruct CMake on how to generate your build files. This is useful if the system has installed multiple compilers.

\section{General Library Layout}

This section will explain the basic layout of the PolyNet library and how the components work together. \autoref{fig:overview} contains the most important classes.

\par PolyNet is based on the server-client architecture, it is not usable for a peer to peer setup. The basic idea behind network games with an authoritative server is that the server and the client run the game simulation. The client only sends its inputs to the server and the server updates the game accordingly. Periodically it sends out the current game state and the clients correct their local simulation by accepting the server state. This way cheating is a lot harder/impossible, since the clients only send inputs and the server can validate those.

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{figures/Overview.png}
	\caption{\label{fig:overview}Simplified Overview PolyNet}
\end{figure}

\begin{description}
\item [PolyNet] This class contains the logic to initialize and de-initialize PolyNet.
\item [SimServer] This class is the central component of the library and contains the game logic written by the user. Since the game-logic is shared between the server and the client, the SimServer class is used by those other two components. This abstract base class has to be inherited from by the user.
\item [Client] This class is a container for the client-side SimServer instance. It also includes functions to send packets to the server. 
\item [Server] This class is also a container for the SimServer. Furthermore the server contains logic to handle clients connecting and managing existing connections.
\item [NetObject] This class is an abstract base class that is recommended to be used as a base for creating custom objects that need to be send over the network.
\item [Input] This class is a NetObject based class that is used for sending player inputs.
\item [Snapshot] This class is used as an abstract base class by the user for custom snapshots. A snapshot represents a gamestate.
\item [Packet] This class is an abstract superclass which is utilized by the user to create custom packet types for sending snapshots and inputs.
\item [NetIdPacket] The NetIdPacket is a special subclass of the packet class. It is used by PolyNet to track and manage unique net-objects.
\end{description}

As you can see most of the classes are abstract and the user has to implement specific functionality by himself/herself. This enables PolyNet to be used in a wide range of different game types/scenarios.

\section{Important Library Components}

This section will explain the most important components and their interfaces. This will also show what the most important functions do or what they should do after being implemented by the user. This will not explain how to use PolyNet (see Appendix) or how PolyNet works in detail.

\subsection{The Packet Class}

A packet is the object that is send over the network by the library. It consists of raw data and an id so the receiver can identify the packet type and process it accordingly. A subclass contains additional members that represent the packet type specific data. For example if the user defined a new input type (e.g. KeyboardInput), a custom packet has to be created to contain a KeyboardInput object. The custom packet has to provide functions to serialize/deserialize the according packet object from a data buffer or to a given buffer.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/Packet.png}
	\caption{\label{fig:packet}Simplified Packet Class}
\end{figure}

\par
Explanation of Packet class shown in \autoref{fig:packet}:
\begin{description}
\item [packet\_id\_] The id identifying the packet type
\item [data\_] The buffer used to serialize the packet
\item [Deserialize] Reads the packet from a buffer. (provided by user)
\item [Serialize] Writes the packet to a buffer. (provided by user)
\item [GetPacketId] A static method to determine the packet type from a buffer
\end{description}

\subsection{The NetObject Class}

A net-object is an object, that is supposed to be send over the network inside of a packet. PolyNet uses the NetObject for inputs and snapshots, since those need to be written into packets. Net-objects are not mandatory to use. But snapshots need to be cloneable and all contained objects need to be too. This is achieved by using the NetObject class and not using a custom solution. A net-object can also be uniquely identified by an id.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/NetObject.png}
	\caption{\label{fig:netobject}Simplified NetObject Class}
\end{figure}

\par
Explanation of NetObject class shown in \autoref{fig:netobject}:
\begin{description}
\item [NetId] This is an unique id associated with this object.
\item [GetNetSize] The function should return the size of the netobject in bytes. (provided by user)
\item [Serialize] Writes the packet to a buffer (provided by user)
\item [Deserialize] Reads the packet from a buffer (provided by user)
\item [Clone] Creates a copy of the object with every field cloned (provided by user)
\end{description}


\subsection{The Input Class}

The Input class is the baseclass for all custom inputs. The library uses this data internally to progress and rewind the game state. A user creating a first person game could for example create a KeyboardInput class (containing key presses) and a MouseInput class (containing mouse movements).

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/Input.png}
	\caption{\label{fig:input}Simplified Input Class}
\end{figure}

\par
Explanation of Input class shown in \autoref{fig:input}:
\begin{description}
\item [PacketId] This is the associated packet id. (for internal use only)
\item [TimeStamp] The point in time the input was performed
\item [ClientId] The client, which performed the input
\end{description}

\subsection{The Snapshot Class}

A snapshot is a time-stamped gamestate. PolyNet requires it to also include the duration, but this is set by the library itself. The user has to create one childclass from this to represent the state of the game being created. The custom snapshot should use net-objects and basic/plain data types as members, so the user can utilize PolyNets built-in serialization.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/Snapshot.png}
	\caption{\label{fig:snapshot}Simplified Snapshot Class}
\end{figure}

\par
Explanation of Snapshot class shown in \autoref{fig:snapshot}:
\begin{description}
\item [TimeStamp] This marks the start time of the snapshot.
\item [Duration] This is the time this snapshot covers.
\item [Inputs] The snapshot contains all valid inputs performed by the clients.
\end{description}

\subsection{The SimServer Class}

The SimServer class is the central class of PolyNet. It performs all the actions to run, rewind, predict, interpolate and check the gamestate on the server and the clients. This class contains a lot of functions that have to be implemented by the user. It also contains methods only used/called when running as the server (name begins with a small 's') or as a client (name begins with a small 'c').

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{figures/SimServer.png}
	\caption{\label{fig:simserver}Simplified SimServer Class}
\end{figure}

\par
Explanation of SimServer class shown in \autoref{fig:simserver}:
\begin{description}
\item [isServer\_] This is a boolean the user can query to determine, if the current SimServer instance is running on a server or client.
\item [SimServer] The constructor is used to tell the sim-server, if it is running on a server and to set a fine simulation interval, which is used for rewinding and rerunning the simulation.
\item [sOnClientConnected] This function is called on the server when a client has connected. The user can use this to accept/decline connects. In here you would for example start the game when a required number of players has connected. (provided by user)
\item [OnPacketReceived] This is called when the clients/server receives a packet. (provided by user)
\item [cSendToServer] Utilized by the user to send packets from the client to the server.
\item [OnStart] Called on the client/server when the server started the game. The user could for example initialize the game state here. (provided by user)
\item [Update] This has to be called by the user to update the client/server.
\item [GetCurrentSnapshot] Returns the current snapshot; e.g. used for grabbing the game state on the client for rendering
\item [GetIsRunning] Returns, whether the SimServer and the network loop is running
\item [UpdateClientAndRelatedObjects] This function has to update the provided client and all associated objects in the snapshot. (provided by user)
\item [UpdateSnapshotWithoutClients] This function has to update all objects in the game state that were not created by user action. (provided by user)
\item [ApplyInput] Applies the input to the provided snapshot. (provided by user)
\item [SendInput] The function has to be called by the user, if the user performs an input. (e.g. keypress, mouse movement)
\item [CreateSnapshot] This function has to return a new custom snapshot. (provided by user)
\item [CreateSnapshotPacket] This function has to return a custom snapshot packet.
\item [sProcessIncomingInput] This function is being called on the server when a new input arrives.
\item [cProcessIncomingSnapshot] This function is being called on the client, if a new snapshot arrives.
\item [ConsumeId] Has to be called by the user when creating new net-objects
\item [GetAllClientIds] Returns all client ids from the snapshot
\end{description}

